const bodyParser = require("body-parser");
const admin = require('firebase-admin')
const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')

const app = express()
const port = 3000

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors())


function getToken() {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 40; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var serviceAccount = require("./miniassingment-firebase-adminsdk-9a56y-247e9c254f.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),  
  apiKey: "AIzaSyAA16PTV0OSiDJC8ciiyooX9fmoLkQ5Y2U",
  authDomain: "assignment4-a9c56.firebaseapp.com",
  projectId: "assignment4-a9c56",
  storageBucket: "assignment4-a9c56.appspot.com",
  messagingSenderId: "861506718380",
  appId: "1:861506718380:web:4d024182725c4aee506d09"
});

const db = admin.firestore();
 

app.get('/', (req, res) => {
    console.log(getToken())
    return res.status(200).send('Hello World!');
});

app.post('/register', (req,res) => {
    (async () => {
        try {
            await db.collection('users').doc("/"+req.body.username+"/").create({
                    nama_lengkap: req.body.nama_lengkap,
                    npm: req.body.npm,
                    username: req.body.username,
                    password: req.body.password,
                });
            return res.status(200).send("User Created");
        } catch (error) {
            console.log(error);
            return res.status(500).send("User Exist");
        }
    })();
})

app.post('/oauth/token', (req, res) => {
    var token = getToken()
    var userRef = db.collection("users").doc(req.body.username);
    var clientRef = db.collection("client").doc(req.body.client_id)
    var accessTokenRef = db.collection("tokens").doc((req.body.client_id+req.body.username))

    userRef.get().then((user) => {
        if (user.exists && user.data().password === req.body.password) {
            userRef.set({
                access_token: token
            }, {merge: true})
            clientRef.get().then(client => {
                if (client.exists){
                    var client_users = client.data().users
                    clientRef.set({
                        users: [
                            ...client_users, {
                                npm: user.data().npm,
                                nama_lengkap: user.data().nama_lengkap,
                                username: user.data().username,
                            }
                        ]}, {merge: true}
                    )   
                } else{
                    clientRef.set({
                        users: [
                            {
                                npm: user.data().npm,
                                nama_lengkap: user.data().nama_lengkap,
                                username: user.data().username,
                            }
                        ],
                            grant_type: req.body.grant_type,
                            client_id: req.body.client_id,
                            client_secret: req.body.client_secret,
                        }, {merge: true}
                    ) 
                };
                accessTokenRef.set({
                    access_token: token,
                    username: user.data().username,
                    client_id: client.data().client_id
                })
                res.json({
                    "access_token" : token,
                    "expires_in" : 300,
                    "token_type" : "Bearer",
                    "scope" : null,
                    "refresh_token" : getToken()
                })
            })
            .catch(err => {
                console.log(err)
                res.json({
                    "error":"invalid_request",
                    "Error_description":"ada kesalahan masbro!"
                })
            })
        } else {
            res.json({
                "error":"invalid_request",
                "Error_description":"ada kesalahan masbro!"
            })
        }
    }).catch((error) => {
        res.json({
            "error":"invalid_request",
            "Error_description":"ada kesalahan masbro!"
        })
    });
})

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})


function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined') {
        console.log("verifying token")
        const bearer = bearerHeader.split(' ')
        const bearerToken = bearer[1]
        var tokenRef = db.collection("tokens");
        tokenRef.where("access_token", "==", bearerToken).get().then(tokens => {
            tokens.forEach(token => {
                next();
            })
            .catch(err => {
                res.send("Bad token")
            })
        })
        .catch(err => res.send("Authorization Failed"));
        
    } else{
        res.status(403).send({
            "error" : "unauthorized"
        })
    }
}

// app.use('/oauth/resource', verifyToken)

app.get('/oauth/resource', (req, res) => {
    const bearerHeader = req.headers['authorization'];
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1]
    var i = 0
    var tokenRef = db.collection("tokens");
    tokenRef.where("access_token", "==", bearerToken).get()
    .then((tokens) => {
        tokens.forEach(token => {
            var userRef = db.collection("users");
            userRef.where("username", "==", token.data().username).get()
            .then((users) => {
                users.forEach((user) => {
                    res.status(200).send({
                        "access_token" : user.data().access_token,
                        "client_id" : token.data().client_id,
                        "user_id" : user.data().username,
                        "full_name" : user.data().nama_lengkap,
                        "npm": user.data().npm,
                        "expires" : 300,
                        "refresh_token" : getToken()
                    })
                });
            })
            .catch(err => {
                res.send("Internal error")
            })
        })
    })
    .catch(err => {
        res.send("Internal Server Error")
    })
})
